#!bin/bash

sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install curl -y
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

# sudo gitlab-runner register
# sudo gitlab-runner start
# sudo usermod -G docker gitlab-runner
# sudo passwd root
# passwd gitlab-runner
# docker login